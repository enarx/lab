#!/usr/bin/python3

import time
import sys
import os

def read(path):
    with open(path) as f:
        return f.read()

def is_slave(disk):
    for dev in os.listdir('/sys/block/'):
        slaves = f'/sys/block/{dev}/slaves'
        if os.path.exists(slaves):
            for slave in os.listdir(slaves):
                if slave == disk:
                    return True
    return False

for exponent in range(0, 6):
    # If any raid devices appear, exit.
    for disk in os.listdir('/sys/block'):
        if os.path.exists(f'/sys/block/{disk}/md'):
            sys.exit(0)

    # If the data partition appears, exit.
    if os.path.exists('/dev/disk/by-label/data'):
        sys.exit(0)

    # Give the disks time to settle.
    time.sleep(2 ** exponent)

# Sort disks into pools by kind (ssd/hdd) and size.
# Disks with the same kind and size are in a pool.
pools = {}
for disk in os.listdir('/sys/block'):
    size = int(read(f'/sys/block/{disk}/size')) * 512
    rdwr = int(read(f'/sys/block/{disk}/ro')) == 0
    plug = int(read(f'/sys/block/{disk}/removable')) != 0
    spin = int(read(f'/sys/block/{disk}/queue/rotational')) != 0
    nvme = os.path.exists(f'/sys/block/{disk}/nsid')
    if not os.path.exists(f'/sys/block/{disk}/device') or not rdwr or plug:
        continue

    kind = 'nvme' if nvme else 'hdd' if spin else 'ssd'
    pools.setdefault(kind, {}).setdefault(size, []).append(disk)

# Create RAIDs on the naked disks
for (kind, pool) in pools.items():
    for (i, (size, disks)) in enumerate(pool.items()):
        # Calculate the RAID level and device nodes.
        level = 5 if len(disks) > 2 else 1
        disks = [f"/dev/{disk}" for disk in disks]
        if len(disks) == 1:
            disks += ["missing"]
        
        cmd = f"mdadm -C /dev/md/{kind}{i} -R -l {level} -n {len(disks)} {' '.join(disks)}"
        print(cmd, file=sys.stderr)
        assert(os.system(cmd) == 0)

# Inventory all the raids
raids = []
for disk in os.listdir('/sys/block'):
    size = int(read(f'/sys/block/{disk}/size')) * 512
    rdwr = int(read(f'/sys/block/{disk}/ro')) == 0
    spin = int(read(f'/sys/block/{disk}/queue/rotational')) != 0
    ismd = os.path.exists(f'/sys/block/{disk}/md')
    if not ismd or not rdwr:
        continue

    slaves = os.listdir(f'/sys/block/{disk}/slaves')

    nvme = True
    for slave in slaves:
        if not os.path.exists(f'/sys/block/{slave}/nsid'):
            nvme = False

    kind = 2 if nvme else 0 if spin else 1
    raids.append((kind, len(slaves), size, disk))

# Sort: NVME<SSD<HDD, then largest first
for (kind, nslaves, size, disk) in reversed(sorted(raids)):
    cmd = f'mkfs.ext4 -F -L data /dev/{disk}'
    print(cmd, file=sys.stderr)
    assert(os.system(cmd) == 0)
    sys.exit(0)

sys.exit(1)
