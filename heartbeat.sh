#!/bin/bash

DASHBOARD=enarx
HOSTNAME="$(hostname)"
IP="$(getent hosts ${HOSTNAME} | awk '{print $1 }')"
PORT=22
CONNECT_TIMEOUT=5
WARNING="5m"
ERROR="10m"


timeout ${CONNECT_TIMEOUT} bash -c "</dev/tcp/${IP}/${PORT}"
if [ $? == 0 ];then
   systemd-cat -p info echo "SSH Connection to ${HOSTNAME} over port ${PORT} is possible"
   systemd-cat -p debug --stderr-priority=err \
       curl -s -X POST  "https://${DASHBOARD}.heartbeat.sh/beat/${HOSTNAME//./_}?warning=${WARNING}&error=${ERROR}"
else
   systemd-cat -p err echo "SSH connection to ${HOSTNAME} over port ${PORT} is not possible"
fi

